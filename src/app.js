const http = require('http');
const express = require('express');
const socketIo = require('socket.io');

const Config = require('./core/config');
const runLoaders = require('./core/loaders');
const registerGuards = require('./core/guards');
const registerRoutes = require('./core/router');
const errorHandler = require('./core/middleware/error-handler');

module.exports = class PlaanetApp {
  constructor() {
    console.log('INFO: Starting Plaanet Instance ...');
    const app = express();
    const httpServer = http.createServer(app);
    const wsServer = http.createServer(app);
    const ioSocket = socketIo(wsServer);
  
    this.context = {
      expressApp: app,
      httpServer: httpServer,
      wsServer: wsServer,
      ioSocket: ioSocket,
    };
  
    this.init();
  }

  async init() {
    console.log('INFO: Registering guards ...');
    await registerGuards(this.context);
  
    console.log('INFO: Running loaders ...');
    await runLoaders(this.context);
  
    console.log('INFO: Registering routes ...');
    await registerRoutes(this.context);

    console.log('INFO: Registering error handler ...');
    this.context.expressApp.use(errorHandler());
  
    await this.context.httpServer.listen(Config.HTTP_PORT);
    console.log(`INFO: API started on http://127.0.0.1:${Config.HTTP_PORT}`);
    
    await this.context.wsServer.listen(Config.WS_PORT);
    console.log(`INFO: WS started on ws://127.0.0.1:${Config.WS_PORT}`);
  }
}

