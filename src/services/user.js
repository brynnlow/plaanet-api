const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const Config = require('../core/config');
const { EntityDoesNotExistsError } = require('../core/errors');

const { User } = require('../models/User');

module.exports = class UserService {

  constructor() { }

  async countUsers() {
    return User
      .countDocuments();
  }

  async getUserById(id) {
    return User
      .findById(mongoose.Types.ObjectId(id))
      .select('-password')
      .populate('pages')
      .exec();
  }

  async getUserByIdWithPassword(id) {
    return User
      .findById(mongoose.Types.ObjectId(id))
      .exec();
  }

  async getRedactedUserById(id) {
    return User
      .findById(mongoose.Types.ObjectId(id))
      .select('_id name pages')
      // Only populate profil page
      .populate('pages', '_id uid slug', null, { type: 'user', owner: mongoose.Types.ObjectId(id) })
      .exec();
  }

  async getUserByName(name) {
    return User
      .findOne({ name })
      .select('-password')
      .exec();
  }

  async compareUserPassword(id, password) {
    const user = await this.getUserByIdWithPassword(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', id);
    }

    return bcrypt.compare(password, user.password);
  }

  async generateTokenForUser(id) {
    const user = await this.getUserById(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', id);
    }

    const payload = {
      _id: id,
      username: user.name,
      role: user.isAdmin ? 'admin' : 'user',
    };

    return jwt.sign(payload, Config.Secrets.COOKIES_KEY);
  }

  async isUserAdmin(id) {
    const user = await this.getUserById(id);

    if (user.isAdmin === true) {
      return true;
    }

    return false;
  }

  async setLastActivity(id, date) {
    const user = await this.getUserById(id);
    user.lastActivityDate = date;
    await user.save();
  }

}