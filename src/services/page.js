const UserService = require('./user');
const Page = require('../models/Page');
const Live = require('../models/Live');
const Instance = require('../models/Instance');

const { EntityDoesNotExistsError } = require('../core/errors');
const core = require('../core/core');
const axios = require('axios');
const RoomService = require("../services/room");

module.exports = class PageService {

  constructor() {
    this.userService = new UserService();
  }

  async getPageById(id) {
    return Page
      .findById(id)
      .exec();
  }

  async getPageByUid(uid) {
    return Page
      .findOne({ uid })
      .exec();
  }

  async getPageProfile(uid, userPageCoordinates) {
    let page = await Page.findOne({ uid: uid })
                         .populate("owner")
    //console.log("getPageProfile", page)
    /*if(page == null)
        page = await Page.findOne({ slug: uid })
                         .populate("owner")
    */
        
    if(page == null) return null

    if(page.type == "assembly" && userPageCoordinates != null){
        const roomService = new RoomService()
        page.canSendSurvey = roomService.canSurvey(page, userPageCoordinates)
    }
    return page
  }

  async getPageForUser(id) {
    const user = await this.userService.getUserById(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', 1);
    }

    return Page
      .findOne({
        type: "user",
        owner: user._id,
      })
      .exec();
  }

  async getPagesForUser(id) {
    const user = await this.userService.getUserById(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', id);
    }

    return Page
      .find({ owner: user._id })
      .exec();
  }

  async getMemberPages(pageUid){
    //get pages where pageUid is in roles.editors
    //test test test test test test test
    console.log("-------------------------")
    //console.log("#PageService.getMemberPages(pageUid)", pageUid)
    
    try{ //broadcast request (to each instance of the network)
      let res = null
      await core.broadcaster.post('/broadcast/get-member-pages', 
            { pageUid: pageUid
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    res = dataRes //.sort((x, y) => { return (x.alert === y.alert)? 0 : x.alert? -1 : 1; })
                }else{
                    console.log("error 1 : dataRes == ", dataRes)
                    res = false
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch")
            }, 'pages') //dataRes[id] to concat for each response after forward
      return res
    }catch(e){
        console.log("error 3")
        return false
    }
  }

  async getBroadPage(pageUid){
    let nodePath = await core.getNodePath(pageUid)
    //console.log("getBroadPage, nodePath ?", nodePath, 'pageUid ?', pageUid)
    try{
      core.broadcaster.init()
      let res = await axios.post(nodePath + '/broadcast/page-profil', 
                                { pageUid : pageUid, userPageCoordinates: null })
      //console.log("getBroadPage",  res.data)
      if(res.data.error == false) return res.data.page
      else return null //or null ? or false ? or error ?
    }catch(e){
      console.log("error getBroadPage", e)
    }
  }

  insertRole(arr, role, identity){
    let found = false
    arr[role].forEach((id, x) => {
      if(id.uid == identity.uid) found = true
    })
    if(!found) arr[role].push(identity)
    return arr
  }

  deleteRole(page, type, role, identity){
    if(role == "admin" && type == 'roles' && page[type][role].length == 1) return page
    let found = -1
    page[type][role].forEach((id, x) => {
      if(id.uid == identity.uid) found = x
    })

    console.log("deleteRole", page.uid, type, role, page[type][role], found)
    if(found > -1) page[type][role].splice(found, 1)

    return page[type]
  }

  deleteRoleTrace(page, type, role, nodeName){
    console.log("deleteRoleTrace", page.uid, type, role, nodeName)
    let regxNodeName =  new RegExp(".*"+nodeName, "i")
    let found = -1
    page[type][role].forEach((id, x) => {
      if(regxNodeName.test(id.uid)) found = x
    })

    console.log("role found", found > -1)
    if(found > -1) page[type][role].splice(found, 1)
    if(found > -1) console.log("role deleted")

    return page[type]
  }

  isRole(arr, role, identity){
    console.log("isROLE ?", arr[role], role, identity)
    let found = false
    arr[role].forEach((id, x) => {
      if(id.uid == identity.uid) found = true
    })    
    return found
  }

  async createLive(pageUidRef){
    let live = new Live()
    live.parentPageUid = pageUidRef
    live.uid = core.getRandUid(core.iName(pageUidRef))
    live.categories = [
        { uid: core.getRandUid(core.iName(pageUidRef)),
          name: 'Général',
          channels: [
            {   uid: core.getRandUid(core.iName(pageUidRef)),
                name: 'Accueil',
                created: new Date(),
                dateLastMsg: new Date(),
                dateLastOpen: []
            }
          ]
        }
    ]
    await live.save()
    console.log("Create New Live : ", live)
    return live
  }

  async findLiveForPage(pageUid, userPageUid, pageType){
    let pageUidRef = this.getPageUidRef(pageUid, userPageUid, pageType)
    /** TODO : check if user is member or friend */
    let nodePath = await core.getNodePath(pageUidRef)
    let iName = await core.iName(pageUidRef)
    let instance = await Instance.findOne({ isLocal: true })
    
    let live = null
    //console.log("is the good instance ?", instance.name, iName, pageUidRef)
    if(instance.name == iName){
      live = await Live.findOne({ 'parentPageUid' : pageUidRef })
      //console.log("found live in local instance ")
    }else{
      let res =  await core.asyncPost(nodePath + '/broadcast/get-live-data', { pageUidRef: pageUidRef })
      //console.log("res ", nodePath + '/broadcast/get-live-data')
      live = res.data.live //await Live.findOne({ 'parentPageUid' : pageUidRef })
    }
    return live
  }


  getPageUidRef(pageUid, userPageUid, pageType){
    let pageUidRef = pageUid
    if(pageType == "user"){
        /** FORMULE DE CREATION DUID POUR LES CONVERSATIONS PRIVEE ENTRE USER */
        if(pageUid < userPageUid) 
             pageUidRef = pageUid + userPageUid
        else pageUidRef = userPageUid + pageUid
        /** FORMULE DE CREATION DUID POUR LES CONVERSATIONS PRIVEE ENTRE USER */
    }
    return pageUidRef
  }

  
}