const axios = require('axios')
const htmlParser = require('node-html-parser');
const { EntityDoesNotExistsError } = require('../core/errors');


module.exports = class OEmbedService {

  constructor() {
    this.services = {
      spotify: {
        platformName: 'Spotify',
        matches: ['embed.spotify.com/*', 'open.spotify.com/*'],
        endpointUrlFactory: url => `https://embed.spotify.com/oembed?url=${url}`,
      },
      twitter: {
        platformName: 'Twitter',
        matches: ['twitter.com/*/status/*', 'm.twitter.com/*/status/*'],
        endpointUrlFactory: url => `https://publish.twitter.com/oembed?url=${url}`,
      },
    };
  }

  async resolveOEmbedEndpoint(url) {
    const res = await axios.get(url);
    const root = htmlParser.parse(res.data);

    if (url.match(/open\.spotify\.com\/*/i) || url.match(/embed\.spotify\.com\/*/i)) {
      return this.services.spotify.endpointUrlFactory(url);
    } else if (url.match(/twitter\.com\/.*\/status\/.*/i) || url.match(/m\.twitter\.com\/.*\/status\/.*/i)) {
      return this.services.twitter.endpointUrlFactory(url);
    }

    const oembedTag = root.querySelectorAll('link[type="application/json+oembed"], link[type="text/json+oembed"]');
    if (!oembedTag || !oembedTag[0]) {
      return null;
    }

    return oembedTag[0].getAttribute('href');
  }

  async resolveForUrl(url) {
    const oembedEndpoint = await this.resolveOEmbedEndpoint(url);
    if (!oembedEndpoint) {
      throw new EntityDoesNotExistsError('Cannot resolve oembed endpoint for url.', url);
    }

    const res = await axios.get(oembedEndpoint);
    return res.data;
  }
}