var mongoose = require("mongoose")

var fileSchema = new mongoose.Schema({
    uid: String,
    name: String,
    type: String,
    path: String,
    date: Date,
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Page'
    },
})

var File = mongoose.model('File', fileSchema)
module.exports = File;
