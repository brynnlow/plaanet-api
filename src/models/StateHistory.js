var mongoose = require('mongoose');

var stateHistorySchema = new mongoose.Schema({
    date: Date,
    nbUsers: Number,
    nbPosts: Number,
    nbNotifs: Number,
    nbMobileUsers: Number
});

var StateHistory = mongoose.model('stateHistory', stateHistorySchema);

module.exports = StateHistory;