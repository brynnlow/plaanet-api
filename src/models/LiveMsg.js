var mongoose = require("mongoose")

var liveMsgSchema = new mongoose.Schema({
    uid: String,
    liveUid: String,
    channelUid: String,
    categoryUid: String,
    parentPageUid: String,

    livePageName: String, //not stored in bdd
    livePageType: String, //not stored in bdd
    channelName: String, //not stored in bdd

    text: String,
    author: mongoose.Schema.Types.Mixed,

    created: Date,
    updated: Date,
})

var LiveMsg = mongoose.model('LiveMsg', liveMsgSchema)
module.exports = LiveMsg;
