const rateLimit = require('express-rate-limit');
const MongoStore = require('rate-limit-mongo');

const Config = require('../config');

module.exports = async function ({ expressApp }) {
  const dbUrl = Config.Database.PASS
      ? `mongodb://${Config.Database.HOST}:${Config.Database.PORT}/${Config.Database.NAME}?authSource=admin&readPreference=primary&appName=plaanet-api&ssl=${Config.Database.SECURE}`
      : `mongodb://${Config.Database.HOST}:${Config.Database.PORT}/${Config.Database.NAME}?readPreference=primary&appName=plaanet-api&ssl=${Config.Database.SECURE}`;

  const registerLimiterStore = new MongoStore({
    uri: dbUrl,
    user: Config.Database.USER,
    password: Config.Database.PASS,
    authSource: 'admin',
    collectionName: 'registerLimiterRecords',
  });

  // Create a router rate limiter middleware
  const registerLimiter = rateLimit({
    store: registerLimiterStore,
    windowMs: 60 * 60 * 1000, // 3 minutes
    max: 5, // start blocking after 5 requests
    message: 'Too many accounts created from this IP, please try again after an hour',
  });

  // Rate limit /auth/save router
  expressApp.use('/auth/save/', registerLimiter);
};