const registerLimiterGuard = require('./registerLimiter');

module.exports = async function registerGuards(ctx) {
  // Add register ip limit guard.
  await registerLimiterGuard(ctx);
};