const AuthTokenService = require('../../services/authToken');
const UserService = require('../../services/user');

const authService = new AuthTokenService();
const userService = new UserService();

module.exports = async function lastUserActivity(req, res, next) {
  try {
    next();

    const token = await authService.getTokenFromRequest(req);
    if (!token) {
      return;
    }

    const payload = await authService.getTokenPayload(token);
    if (!payload) {
      return;
    }

    const user = await userService.getUserById(payload._id);
    if (!user) {
      return;
    }

    await userService.setLastActivity(user._id, Date.now());
  } catch (err) {
    // Do nothing
    next(err);
  }
}
