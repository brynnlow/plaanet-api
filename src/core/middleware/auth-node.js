const core = require('../core');
const Instance = require('../../models/Instance')

module.exports = async function(req, res, next) { 
  //get the nodeKey from the header if present
  const nodeKey = req.headers["x-auth-token"];
  //if no nodeKey found, return response (without going to the next middelware)
  if (!nodeKey) {
    console.log("Access denied. No nodeKey provided")
    return res.status(401).send("Access denied. No nodeKey provided")
  }
  try {
    //search the node in local Network
    const instanceName = core.iName(nodeKey)
    const instanceKey = core.iKey(nodeKey)
    let instance = await Instance.findOne({ name: instanceName, nodeKey: instanceKey })
    //if node not exits : denie
    if(instance == null) {
      console.log("Access denied. You are not a valide node", nodeKey)
      return res.status(401).send("Access denied. You are not a valide node")
    }else if(instance.lockIn == true){
      console.log("Access denied. The node ", instanceName, " has been lockedIn by our instance.")
      return res.json({ error : true, errorStatus: 'lockIn'}) //res.status(401).send("Access denied. Your node has been lockedIn by our instance.", instanceName)
    }

    //console.log("Auth Node access valide")
    //if not denied : go next !
    next();
  } 
  catch (ex) {
    console.log('Invalid nodeKey:', req.headers["x-auth-token"])
    res.status(400).send("Invalid nodeKey.");
  }
};