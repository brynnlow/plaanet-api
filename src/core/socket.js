
//const Instance = require('../models/Instance')

const core = require('../core/core');
const axios = require('axios');
const Instance = require('../models/Instance')
const Page = require('../models/Page')

let ws = {
    io: null,
    clients: [],
    init: function(io){
        ws.io = io
        // Quand un client se connecte, on le note dans la console
        ws.io.sockets.on('connect', function (socket) {
            console.log('Un client est connecté !', ws.clients.length);
            socket.emit("conf-connexion", { msg: "hello" })

            socket.on('join', (data) => {
                //delete older socket if exists
                ws.clients.forEach((c,i)=>{ if(c.UID == data.UID) ws.clients.splice(i, 1) })
                //push new socket
                ws.clients.push({ UID: data.UID, socket : socket })
                console.log('-------------------------');
                console.log('JOIN SOCKET !', data.UID);
            });

            socket.on('get-count', (data) => {
                socket.emit("count-connected", { count: ws.clients.length })
                //console.log('count-connected', ws.clients.length);
            });

            socket.on('leave', (data) => {
                console.log('LEAVE !', data.UID);
                //delete older socket if exists
                ws.clients.forEach((c,i)=>{ if(c.UID == data.UID) ws.clients.splice(i, 1) })
                
            });
            socket.on('get-connected', async () => {
                //console.log('get-connected', ws.clients.length)
                let cli = []
                await Promise.all(ws.clients.map(async (c,i)=>{ 
                    let page = await Page.findOne( { uid: c.UID } )
                    if(page != null) cli.push(page) 
                }))
                socket.emit("send-connected", { clients: cli })
            });


        });
    },
    emit: async function(targetUid, action, data){
        //console.log("ws.clients", ws.clients.length, targetUid)
        if(targetUid == null) return
        
        let sock = null
        ws.clients.forEach((c,i)=>{ //console.log("c.UID", c.UID, targetUid)
            if(c.UID == targetUid) sock = c.socket 
        })

        if(sock != null){
            sock.emit(action, data)
            //console.log("local socket send to", action)
        }else{
            let instance = await Instance.findOne({ isLocal:true })
            if(core.iName(targetUid) != instance.name){
                //console.log("the socket target in not on this server", targetUid, ws.clients.length)
                let nodePath = await core.getNodePath(targetUid)
                let url = nodePath + '/broadcast/relay-socket'
                core.post(url, {   targetUid: targetUid, 
                                    action: action, 
                                    data: data
                                },
                    function(dataRes){
                        //console.log("socket send to relay :", url, targetUid)
                    },
                    function(dataRes){
                        console.log("error socket : unreachable relay node", url, targetUid)
                    })
            }
        }
    },
    isOnline: function(uid){
        //console.log("isOnline ?", uid)
        let found = false
        ws.clients.forEach((c,i)=>{ //console.log("c.UID", uid)
            if(c.UID == uid){
                found=true 
                //console.log("found :", found)
                return
            }
        })
        return found
    }
    
}
module.exports = ws;