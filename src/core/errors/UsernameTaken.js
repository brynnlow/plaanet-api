module.exports = class UsernameTakenError extends Error {

  constructor(message) {
    super(message);

    this.name = 'UsernameTakenError';
    this.httpStatusCode = 403;
  }

}