# How to setup a node with Docker Compose

This guide will show you how with minimal knoweldges 
you can run your own Plaanet instance.

Running a Plaanet instance helps the network grow.

The more people hosts a Plaanet node, the more network 
becomes resilient to censorship and attacks.

Plaanet is a federated network and thus needs people to
host a lot of nodes so it stays independant and free!

## Table of contents

* [Pre-requisites](#pre-requisites)
* [Deploy a Plaanet instance](#deploy-a-plaanet-instance)
  - [Installation](#installation)
  - [Configuration](#configuration)
  - [Run the services](#run-the-services)
* [Initialize your Plaanet instance](#initialize-your-plaanet-instance)
  - [Register instance in the Plaanet network](#register-instance-in-the-plaanet-network)
  - [Create the admin user](#create-the-admin-user)
* [Join the instance hoster's Discord](#join-the-instance-hosters-discord)

## Pre-requisites

  - A linux machine (or any machine running docker)
  - Docker installed on the machine
  - A domain name poiting to the machine
  - An email address to receive SSL certs renewal notices


## Deploy a Plaanet instance

### Installation

Run the following commands to clone the Plaanet API on the machine where you decided to host the instance:

```
$ cd $HOME/
$ git clone https://gitlab.com/plaanet/plaanet-api.git
$ cd plaanet-api/
```

### Configuration

In order to make things simple we provides default configuration for almost anything. Defaults are fine but sometimes you may want to customize how you handle things (i.e. database, folders path, etc).

#### Configuration basics

The configuration files are located at these paths:

* `plaanet-api/docker-compose.yml`
* `plaanet-api/config/default.json`
* `plaanet-api/config/production.json`
* `plaanet-api/data/nginx/app.conf.example`
* `plaanet-api/data/nginx/app.conf`

The `docker-compose.yml` file holds some services related configuration. This may be extracted later into an env file to make things easier but for the moment you will need to edit this file directly.

The `default.json` file is a file containing all the available configuration properties we let you to customize. Editing things here has no impact on the final configuration. It's meant to be an "help" for you to craft your own configuration file.

The `production.json` file is where you put the actual configuration for your instance. This is the file that is read by Plaanet API when starting. By default this file doesn't exists and thus you need to create it, copy the `default.json` file into it and customize the way it better fits your case.

The `app.conf.example` is the NGINX config file that defines how users of your instance will access the services provided by Plaanet API. You will need to copy this file and paste it as `app.conf` in the same folder.

#### Required configuration

As said before, we have sane defaults for **almost** every configuration property, but some things needs to be edited by yourself before running the instance. Let's do that!

**Note that you need a domain name already pointing on your machine before performing this step!**

First let set your domain name where it needs to be set!
Replace any occurence of `plaanet.io` by your domain name.

**File: `plaanet-api/data/nginx/app.conf`**  

```diff
server {
  listen 80;
- server_name plaanet.io;
+ server_name your-domain-name.com;

  # ...
}

server {
  listen 443;
- server_name plaanet.io;
+ server_name your-domain-name.com;

- ssl_certificate /etc/letsencrypt/live/plaanet.io/fullchain.pem;
+ ssl_certificate /etc/letsencrypt/live/your-domain-name.com/fullchain.pem;
- ssl_certificate_key /etc/letsencrypt/live/plaanet.io/privkey.pem;
+ ssl_certificate_key /etc/letsencrypt/live/your-domain-name.com/privkey.pem;

  # ...
}
```

**File: `plaanet-api/init-letsencrypt.sh`**  
```diff
#!/bin/bash

# ...

-domains=(plaanet.io)
+domains=(your-domain-name.com)
rsa_key_size=4096 # Do not edit
data_path="./data/certbot" # Do not edit
-email="" # Adding a valid address is strongly recommended
+email="contact@your-domain-name.com"
staging=1 # Set to 1 if you're testing your setup to avoid hitting request limits

# ...
```

**File: `plaanet-api/config/production.json`**
```diff
{
  "env": "dev",
-  "instance_url": "http://192.168.1.69",
+  "instance_url": "https://your-domain-name.com",
-  "instance_port": 3000,
+  "instance_port": 443,
  // ...
}
```

<!-- TODO: Write about other things that can be configured. -->

### Run the services

Once configured you can now run the instance by letting docker-compose do the heavy work for you but before that, you need to generate SSL certs (for free) with LetsEncrypt. We provide you a script that automatize this for you so its as simple as running:

```bash
$ cd plaanet-api
$ ./init-letsencrypt.sh
```

If the process goes well letsencrypt issued you fake certs, then you can edit the `plaanet-api/init-letsencrypt.sh` file to get real certs:

**File: `plaanet-api/init-letsencrypt.sh`**
```diff
#!/bin/bash

# ...

domains=(your-domain-name.com)
rsa_key_size=4096 # Do not edit
data_path="./data/certbot" # Do not edit
email="contact@your-domain-name.com"
-staging=1 # Set to 1 if you're testing your setup to avoid hitting request limits
+staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

# ...
```

The re-run the command to get real SSL/TLS certs:

```bash
$ ./init-letsencrypt.sh
```

Now that we have SSL certs, it's time to actually run the instance!

```bash
$ docker-compose up -d
```

In order to see if it works, you can run this:

```bash
$ docker-compose ps
```

If every service is 'Running' (except maybe certbot one) then you can try to access your instance `https://my-domain-name.com`.

## Initialize your Plaanet instance

If you reached this section, congratulations! You've managed to setup a Plaanet instance and we are thanksfull for this! Now read on to create your admin user and register into the Plaanet network.

### Register instance in the Plaanet network

Ok so now you have a working instance, its time to register it to the network. For the duration of this guide, you've been detached from the network so configuration and trial/errors session doesn't affect existing instances and users.

We need to edit some strings in the configuration to make this a thing of the past and actually connect to the REAL network.

**File: `plaanet-api/config/production.json`**
```diff
{
  // ...
  "root_node": {
-   "url": "http://192.168.1.61",
+   "url": "https://plaanet.io",
-   "port": 3000
+   "port": 444
  },
  // ...
}
```

You can now restart the Plaanet API service via docker-compose so it takes effect!

```bash
$ docker-compose up -d --force-recreate --no-deps api
$ docker-compose logs -f api
```

The server logs will be displayed and you should find something like this:

```
INFO: Init token: bbf9b1d1-542e-4bf9-ae66-60801175408d
```

Copy this init token and share it with NOBODY. It prevents network scanners/bots to discover your instance and initialize it before you do!

Now lead to https://plaanet.io/boot-node and fill the required fields to add your node to the network.

Starting from now, your instance is now a member of the Plaanet network! :D

### Create the admin user

Creating the admin user can be done by doing a `curl` request to the API of your instance:

```bash
# Fill this with your infos
$ export PLAANET_INSTANCE_URL="https://your-domain-name.com"
$ export PLAANET_ADMIN_USER="your-username"
$ export PLAANET_ADMIN_PASS="your-s3cur€-adm1n-p455w0rd*"

# Let's create the admin user
$ curl --request POST \
  --url ${PLAANET_INSTANCE_URL}/auth/register \
  --header 'Content-Type: application/json' \
  --data "{ \"name\": \"${PLAANET_ADMIN_USER}\", \"password\": \"${PLAANET_ADMIN_PASS}\" }"

# For security, remove vars
$ unset PLAANET_INSTANCE_URL
$ unset PLAANET_ADMIN_USER
$ unset PLAANET_ADMIN_PASS
```

## Join the instance hoster's Discord

We have a Discord channel dedicated to instance's hosters. You can request access to it from the general channel and we'll make sure to get you in!

* **Discord invite:** https://discord.gg/CbMBUKy