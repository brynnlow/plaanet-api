const Config = require('../src/core/config');

module.exports = {
  async up(db, client) {
    /*if (!Config.Database.PASS) {
      console.log('Not creating a new user because password is empty in config file.');
      return;
    }

    await db.createUser({
      user: Config.Database.USER,
      pwd: Config.Database.PASS,
      roles: [
        {
          role: "readWrite",
          db: Config.Database.NAME,
        },
      ],
    });

    console.log(`Created a new Mongo user: ${Config.Database.USER}`)*/
  },
  async down(db, client) {
    if (!Config.Database.PASS) {
      // Do not remove non-existant user with empty password...
      return;
    }

    await db.removeUser(Config.Database.USER);
  }
};