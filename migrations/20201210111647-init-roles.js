const Page = require('../src/models/Page')

module.exports = {
  async up(db, client) {

    let dbColName = 'pages'

    // Create roles field if it doesn't exists.
    await db.collection(dbColName)
      .updateMany({
        //roles: { $exists: false }
      }, {
        $set: { roles:        { 'admin': [], 'moderator': [], 'editor': [] }, 
                pendingRoles: { 'admin': [], 'moderator': [], 'editor': [] },
                inviteRoles:  { 'admin': [], 'moderator': [], 'editor': [] }, }
      });

      let node = await db.collection('instances').findOne({ isLocal: true })

      let x = 0
      //all owner are admin, moderator and editors
      let pages = await db.collection(dbColName).find({}).toArray()
      console.log("pages.len", pages.length)
        await Promise.all(pages.map(async (i) => {
          x++
            console.log('search ownerPage', i.name, x)
            const ownerPage = await db.collection(dbColName).findOne({ owner: i.owner, type: 'user' })
            
            if(ownerPage!=null && i.roles['admin'].length == 0){
              //console.log('found ownerPage', ownerPage.name, i.name, i.uid, i.roles['admin'].length)
              //if(i.type != 'user') console.log('page type', i.type, i.name)

              let id = { uid: ownerPage.uid, name: ownerPage.name } 
              if(i.roles['admin'].length == 0)      i.roles['admin'].push(id)
              if(i.roles['moderator'].length == 0)  i.roles['moderator'].push(id)
              if(i.roles['editor'].length == 0)     i.roles['editor'].push(id)
              

              console.log('before updateOne', x, i.type, i.name, i.uid, i.roles['admin'].length)
              try{
                db.collection(dbColName).updateOne({ uid: i.uid }, 
                                                  { $set: { roles: i.roles } })
                console.log('after updateOne', x, i.type, i.name, i.uid)
              }
              catch(e){ console.log("big error", e) }

            }else{
              console.log('X search ownerPage is null or admin set', ownerPage==null, i.name, i.roles['admin'].length)
              
            }
          }))
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  }
};
